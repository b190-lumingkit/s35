const express = require("express");
// this let us use the mongoose module
const mongoose = require("mongoose");

const app = express();
const port = 3000;

// SECTION - MongoDB connection
/* 
    SYNTAX
        mongoose.connect("<MongoDB Atlas connection string/database?>", 
            {
                useNewUrlParser: true,
                useUnifiedTopology: true
            }
        )
*/
// Connect to the database by passing in your connection string (from MongoDB)
// b190-to-do is the database that we have created in our MongoDB
/* 
    ".connect()" lets us connect and access the database that is sent to it via string
*/
mongoose.connect("mongodb+srv://jaycee24:brtMms9x3zndixW@wdc028-course-booking.8oezu.mongodb.net/b190-to-do?retryWrites=true&w=majority",
// this will not prevent Mongoose from being used in application
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

// sets notifications for connection success or failure
// allows us to handle errors when initial connection is established
// works with on and once Mongoose methods
let db = mongoose.connection;
// if a connection error occurred, print in console "connection error"
// console.error.bind allows us to print errors in the browser as well as in the terminal
db.on("error", console.error.bind(console, "connection error"));
// if the connection is successful, confirm/print in the console
db.once("open", () => console.log(`We're connected to the database.`));

// Schema() is a method inside the Mongoose module that let us create schema for our database; it receives an object with properties and data types that each property should have
const taskSchema = new mongoose.Schema(
    {
        // the "name" property should receive a string data type (the data type should be written in Sentence case)
        name: String,
        status: {
            type: String,
            // the default property allows the server to automatically assign a value to the property once the user fails to provide one
            default: "pending"
        }
    }
);

// SECTION - Models
// models in Mongoose use schemas and are used to complete the object instantiation that correspond to that schema
// Models use Schemas and they act as the middleman from the server to the database
// First parameter is the collection in where to store the data
// Second parameter is used to specify the Schema/Blueprint of the documents that will be stored in the MongoDB collection
// Models must be written in singular form, sentence case
// The "Task" variable now will be used for us to run commands for interacting with our database
const Task = mongoose.model("Task", taskSchema);

// allows handling of json data structure
app.use(express.json());
// receives all kinds of data
app.use(express.urlencoded( { extended: true }));

// Create a new task
/* 
    BUSINESS LOGIC
        - add a functionality that will check if there are duplicate tasks
            - if the task is already existing, we return an error
            - if the task is not existing, we add it in the database
        
        - the task will be sent from the request's body
        - create a new Task object with a "name" field/property
        - the "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
*/
app.post("/tasks", (req, res) => 
{
    // findOne is the mongoose method for finding documents in the database. It works similar to .find in Robo3T
    // findOne returns the first document it finds, if there are any; else it will return "null"
    // "err" parameter is for if there are errors that will be encountered during the finding of the documents
    // "result" is fpr if the finding of documents is successful (whether there are documents or none)
    Task.findOne({name: req.body.name}, (err, result) => 
    {
        if (result !== null && result.name === req.body.name) 
        {
            return res.send("Duplicate task found")
        } 
        else 
        {
            let newTask = new Task({
                // detects where to get the properties that will be needed to create the new "Task" object to be stored in the database
                // the code says that the "name" property will come from the request body
                name: req.body.name
            });
            // ".save()" is a method to save the object created in the database
            newTask.save((saveErr, savedTask) => {
                if (saveErr) 
                {
                    return console.error(saveErr)
                } 
                else 
                {
                    return res.status(201).send("New task created.");
                }
            });
        }
    });
});

app.get("/", (req, res) => 
{
    Task.find((err, result) => {
        if (err) {
            return res.send(err);
        } else {
            return res.status(200).send(result);
            // creating a new object with "data" property and the value of the result
            // return res.status(200).send({ data : result })
        }
    });
});


app.listen(port, () => console.log(`Server running at port: ${port}`));



// SECTION - s35 ACTIVITY

// create a Schema for User
const userSchema = new mongoose.Schema(
    {
        username: String,
        password: String,
        dateAdded: {
            type: Date,
            default: Date.now()
        }
    }
);

// instantiate User
const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res) =>
{
    User.findOne({ username: req.body.username }, (err, result) =>
    {
        if (err)
        {
            return console.log(err);
        }
        else if (result !== null && result.username === req.body.username) 
        {
            return res.send(`Username already taken.`);
        } 
        else
        {
            let newUser = new User({
                username: req.body.username,
                password: req.body.password
            });
            newUser.save((saveErr, saveUser) => 
            {
                if (saveErr) 
                {
                    return console.log(saveErr);
                } 
                else 
                {
                    return res.status(201).send(`${saveUser.username} successfully added.`);
                }
            });
        }
    });
});

app.get("/all-users", (req, res) => 
{
    User.find((err, result) => 
    {
        if (err) 
        {
            return console.log(err);
        } 
        else 
        {
            return res.status(200).send(result)
        }
    });
});